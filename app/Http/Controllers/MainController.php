<?php
namespace App\Http\Controllers;

/**
*
* Offer endpoints to incoming requests to LMS
* @param Request $request
* @return mixed
* @author Samuel Ndiba <samuel@codeengine.co.ke>
*
*/

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;

class MainController extends Controller
{

	/**
	* Test function
	*/

    public function index()
    {
        return User::all();
    }

    /**
    * Receives transaction details
    * Validates the details, queues to LMS for processing
    * @param Request
    * @return json response
    *
    */
    public function transaction_inputs(Request $request)
    {

        /**
        * Check if the request is valid
        */

        if(!empty($request -> all()) && is_array($request -> all()))
        {
            /**
            * Expected Parameters 
            */

           $input = $request -> all();

           $transaction_reference = trim($input['transaction_reference']);
           $transaction_type = trim($input['transaction_type']);

           $timestamp = trim($input['transaction_timestamp']);
           $format_date = new \DateTime($timestamp);
           $transaction_timestamp = $format_date -> format('Y-m-d H:i:s');

           $transaction_amount = trim($input['transaction_amount']);
           $account_number = trim($input['account_number']);
           $currency = trim($input['currency']);
           
           /**
           * Compact the fields into a single array 
           */

           $data[] = compact('transaction_reference','transaction_type','transaction_timestamp','transaction_amount','customer_phone','currency');
          
           /**
           * Call the queuing method and pass data array 
           */
           
           return $this -> queue_requests($data);
        }

        /**
        * If the request is invalid return bad request error 
        */

        else
        {
            return $this -> response -> errorBadRequest('Invalid request format');
        }

    }

        /**
        *
        * Method to queue the requests to LMS
        * @param Request $request
        * @return json response
        *
        */

        public function queue_requests($data)
        {
            /* 
            * Dispatch new job to queue worker which will send request to LMS
            */

               //$this -> dispatch((new Queue_lms_requests($data)) -> delay(60 * 0.5));

            /* 
            * Return success message 
            */

            $message = [
                'status_code' => 201,
                'Message' => 'Request successful. Thank you for choosing us.'
            ];

            return $this-> response-> array($message,200 );
        }

}
