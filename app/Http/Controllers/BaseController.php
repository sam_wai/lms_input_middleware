<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Oauth_client;

class BaseController extends Controller
{

	/**
	* Generate access token
	* @param client_id, client_secret
	* @return access_token
	*/

    public function authenticate(Request $request)
    {
    	/**
    	* Check user credentials against the database
    	*/

    	$client = $request -> all();

    	$client_id = $client['client_id'];
    	$client_secret = $client['client_secret'];

    	try
    	{
    		$db_client = Oauth_client::where(['id' => $client_id, 'secret' => $client_secret])
    	                         ->firstOrFail();

	    	/**
	    	* Check if client is active
	    	*/

	    	if($db_client -> active)
	    	{

                if($db_client -> authorized)
                {
                	/**
			    	* If ok return access token
			    	*/

		    		return \Response::json(\Authorizer::issueAccessToken());

                } 
                /**
                 * Notify the API call that its not authorized
                */

		    	else
		    	{
		    		$response = [
	                        'status_code' => 403,
	                        'error' => true,
	                        'message' => "API call rejected.",
	                        'help' => "Ask CE Mobility Cloud admin to authorize this Consumer Key",
	                        'data' => []
                    ];

                    return $response;
		    	}


	    	}
	    	  /**
              * Notify the API call that client not active
             */
	    	else
	    	{
	    		
	    		$response = [
	                    'status_code' => 403,
	                    'error' => true,
	                    'message' => "API call rejected. Client not activated. Contact CE Mobility cloud customer support",
	                    'help' => "Contact CE customer support with $client_id as issue",
	                    'data' => []
                    ];

                return $response;    
	    	}
        
    	} catch(\Illuminate\Database\Eloquent\ModelNotFoundException $e){

             $response = [
                'status_code' => 403,
                'error' => true,
                'message' => "API call rejected. Invalid login credentials.",
                'help' => 'Invalid API credentials. Confirm from Mobility dashboard',
                'data' => []
            ];

            return $response;
    	} 	
    }
}
