<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$api = app('Dingo\Api\Routing\Router');

/**
* Unprotected api routes
*/

$api->version('v1', ['namespace' => 'App\Http\Controllers'], function ($api) {
	
	$api -> post('gateway/v1/authenticate', 'AuthenticateController@authenticate');	 		

});

/**
* Protected api routes
*/

$api->version('v1', ['middleware' => 'oauth','namespace' => 'App\Http\Controllers'] , function ($api) {
	/**
	* Test routes 
	*/
     $api->get('users', 'MainController@index');
   
     $api->get('time', function () {
         return ['now' => microtime(), 'date' => date('Y-M-D',time())];
     });

     $api -> post('gateway/v1/transaction', 'MainController@transaction_inputs');

});
