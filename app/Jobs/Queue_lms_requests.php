<?php

namespace App\Jobs;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Psr7;

class Queue_requests_to_lms extends Job implements ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */

    $protected $data;

    public function __construct($data)
    {
        $this -> data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        
     /**
      * Initialize guzzlehttp to make contact with the LMS Api
      */

      $client = new Client([
            'base_uri' => 'http://localhost:8001/', //configurable api route
            'timeout' => 2.0
        ]);

        /**
        * Try the guzzlehttp execution
        */

         try
         {
            /**
            * Create a request to LMS Api and pass the request data
            */

            $request = $client -> post('test',['json' => ['data' => $data]]);

            $response_code = $request -> getStatusCode();

             /**
            * If request to LMS Api successful, get the response and send to the notification url
            */

            if ($response_code == 200)
            {
              $response = $request -> getBody() -> getContents();

              // send the response to the notification url
            }

            /**
            * If not successfull return error response
            */

            else
            {
              return $this -> response -> error('Request execution was unsuccesfull');
              exit;
            }
    }
}
